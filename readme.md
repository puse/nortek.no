## Python Crash Course

### dict (Dictionary)
Se pre_data definert i *csv2db.py:75.*
Dicts i python er hash tables, har en syntax som er veldig lik JSON.

    {
       'lok_enhet2': 'M3',
       'lok_nr': '10362',
       'species': 'Regnbueørret',
       'lok_navn': 'NERFJØSET',
       'lok_plass': 'LAND',
       'till_kap': '0,00',
       'purpose': 'Kommersiell',
       'till_enhet': 'TN',
       'production_form': 'MATFISK',
       'end_date': '',
       'lok_kap': '100,00',
       'geowgs84_n': '59.668083',
       'lok_kom': 'ÅS',
       'weatherenv': 'FERSKVANN',
       'start_date': '03-10-1991',
       'till_kom': 'ÅS',
       'lok_komnr': '0214',
       'geowgs84_e': '10.758450\n',
       'till_komnr': '0214',
       'till_nr': 'A A 0001'
    }

Er fleksibel når det kommer til types, man kan feks lage en dict som ser slik ut:

    {
       1: "foo", # <int> <string>
       "bar": 42 # <string> <int>
    }

### Generator
Se json_ready definer *models\organization.py:28*
Handy måte å genere lister på. I eksemplet itererer loopen over alle elementer
i permissions_copy, som er en liste av Permission objecter. Henter en verdi ut
fra `__dict__` feltet, som er et innebygd felt so retunerer alle selv-definerte
felt som et dict av type `<string> <object>` der nøkkelen er navnet på feltet.

### `__init__.py`
Filer gjør en mappe om til en python pakke. Kan innholde kode, som i tilfelle til *nortek\__init__.py*
Koden gjøres om man importer en mappe. og ikke en fil.


## Resultat
