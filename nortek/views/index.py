from flask import *
from nortek.database import db_session
from nortek.models.organization import Organization


mod = Blueprint('index', __name__)


@mod.route("/")
def index_view():
    return render_template("index.html")

