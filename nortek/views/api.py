from flask import *
from nortek.database import db_session
from nortek.models.organization import Organization
from nortek.models.permission import Permission

mod = Blueprint('organization', __name__)


@mod.route("/organization/<column>/<query>")
def organization_view(column, query):
    """
    Returns the a list of organization objects (JSON) that are found using the supplied query.

    :param column:
    :param query:
    :return:
    """
    found = db_session.query(Organization).filter(getattr(Organization, column).like("%" +query+ "%")).all()

    json_response = {}

    if found is not None:
        json_response["data"] = [obj.to_dict() for obj in found]

    return jsonify(json_response)


@mod.route("/permissions/<org_id>/<int:limit>/<int:page>")
def permissions_view(org_id, limit, page):
    query = db_session.query(Permission).filter(Permission.organization_id == org_id)
    query = query.limit(limit)
    query = query.offset(page*limit)

    found = query.all()

    json_response = {}

    if found is not None:
        json_response["data"] = [obj.to_dict() for obj in found]

    return jsonify(json_response)
