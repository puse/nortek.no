from nortek import app
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# sqlite is fine.
engine = create_engine("sqlite:///C:\\Users\\puse\\PycharmProjects\\nortek.no\\dev.db", convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Model = declarative_base()
Model.query = db_session.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import nortek.models.organization
    import nortek.models.permission

    Model.metadata.create_all(bind=engine)


def drop_all():
    Model.metadata.drop_all()