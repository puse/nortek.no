from flask import *
from  nortek.database import db_session


app = Flask(__name__)
app.config.from_object('config')


@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()



# Register views.
from nortek.views import api
app.register_blueprint(api.mod)

from nortek.views import index
app.register_blueprint(index.mod)

if __name__ == '__main__':
    app.run(debug=True)


