/**
 * Created by puse on 23.05.2017.
 */
$(document).ready(function(){



    $('#theb').on('click',function(){
        var map_object = $('#world-map').vectorMap('get', 'mapObject');
        var column = $("#column option:selected").attr("value");
        var query = $("#query").val();

        $.getJSON("/organization/"+column+"/"+query, function(data) {
            var index = 0;
            data["data"].forEach(function (org) {
                var $org_div = populate_org_div(org);

                get_permissions(org, $org_div);
            });
        });
    });
});


function get_permissions(org_data, $org_div) {
    var map_object = $('#world-map').vectorMap('get', 'mapObject');

    $.getJSON("/permissions/"+org_data["id"]+"/10/0", function(data) {
        var index = 0;
        data["data"].forEach(function (per) {
            var $per_div = populate_per_li(per, $org_div);

            map_object.addMarker(index, {latLng: [per["geowgs84_n"], per["geowgs84_e"]], name:per["species"]});
            index++;
        });
    });
}


function populate_org_div(org_data) {
    // Clone this div that is hidden, it holds the structure we want to display the information.
    var $org_div = $(".org").clone();

    $org_div.find(".org_name").text(org_data["name"]);
    $org_div.find(".org_nr").text(org_data["org_nr_pers_nr"]);
    $org_div.find(".org_post_code").text(org_data["post_code"]);
    $org_div.find(".org_post_place").text(org_data["post_place"]);
    $org_div.find(".org_address").text(org_data["address"]);

    // This speeds up the clone action when selecting the hidden div.
    $org_div.removeClass("org");
    $org_div.appendTo("#thelist");
    $org_div.show();

    return $org_div;
}

function populate_per_li(per_data, $org_div) {
     // Same thing here.
    var $per_div = $(".permissions_li").clone();

    $per_div.find(".pre_purpose").text(per_data["purpose"]);
    $per_div.find(".pre_production_form").text(per_data["production_form"]);
    $per_div.find(".pre_species").text(per_data["species"]);
    $per_div.find(".pre_weatherenv").text(per_data["weatherenv"]);

    // This speeds up the clone action when selecting the hidden li.
    $per_div.removeClass("permissions_li");
    $per_div.appendTo($org_div.find(".org_permissions_list"));
    $per_div.show();

    return $per_div;
}