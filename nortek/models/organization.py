from sqlalchemy.orm import relationship
from nortek.database import Model
from sqlalchemy import Column, Integer, String
from nortek.models.permission import Permission


class Organization(Model):
    __tablename__ = 'organization'
    id = Column('id', Integer, primary_key=True)
    org_nr_pers_nr = Column(String(64), default="Sensitiv Opplysning")
    name = Column(String(128))
    address = Column(String(128))
    post_code = Column(String(32))
    post_place = Column(String(64))

    # one to many
    permissions = relationship("Permission")

    def __init__(self, args):
        for attr, value in args.items():
            setattr(self, attr, value)

    def to_dict(self):
        """
        Returns the object as a dict, cant use __dict__ because of SA.
        :return:
        """
        # Copy list, then remove SA reference, it is not serializable
        permissions_copy = self.permissions[:]
        json_ready = [p.__dict__ for p in permissions_copy]
        for p in json_ready:
            p.pop("_sa_instance_state", None)

        return {
            "id": self.id,
            "org_nr_pers_nr": self.org_nr_pers_nr,
            "name": self.name,
            "address": self.address,
            "post_code": self.post_code,
            "post_place": self.post_place,
            "permissions": json_ready,
        }

    @staticmethod
    def create_obj(args):
        return Organization(
            args
        )


