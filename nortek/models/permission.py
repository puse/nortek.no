from nortek.database import Model

from sqlalchemy import create_engine, Column, Integer, String, DateTime,\
    ForeignKey, event


class Permission(Model):
    __tablename__ = 'permission'
    id = Column('id', Integer, primary_key=True)

    till_nr = Column(String(64))

    start_date = Column(String(64))
    end_date = Column(String(64))

    till_komnr = Column(String(64))
    till_kom = Column(String(64))
    purpose = Column(String(128))
    production_form = Column(String(128))
    species = Column(String(64))
    till_kap = Column(String(64))
    till_enhet = Column(String(64))
    lok_nr = Column(String(64))
    lok_navn = Column(String(128))
    lok_komnr = Column(String(64))
    lok_kom = Column(String(64))
    lok_plass = Column(String(64))
    weatherenv = Column(String(64))
    lok_kap = Column(String(64))
    lok_enhet2 = Column(String(64))
    geowgs84_n = Column(String(64))
    geowgs84_e = Column(String(64))

    organization_id = Column(Integer, ForeignKey("organization.id"))

    def to_dict(self):
        as_dict = self.__dict__
        as_dict.pop("_sa_instance_state", None)

        return as_dict

    def __init__(self, args):
        for attr, value in args.items():
            setattr(self, attr, value)

    @staticmethod
    def create_obj(args):
        return Permission(
            args
        )