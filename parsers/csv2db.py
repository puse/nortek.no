from nortek.models.organization import Organization
from nortek.models.permission import Permission
from nortek.database import db_session



#C:\\Users\\puse\\PycharmProjects\\nortek.no\\parsers\\data\\Akvakulturregisteret.csv



items = {
    "org_nr_pers_nr": {},
    "name": {},
}


# pre_headers and org_headers servers as selectors for the csv data. Each tuple pair is <key> <index>
pre_headers = [
    ("till_nr", 0),
    ("start_date", 6),
    ("end_date", 7),
    ("till_komnr", 8),
    ("till_kom", 9),
    ("purpose", 10),
    ("production_form",11),
    ("species", 12),
    ("till_kap", 13),
    ("till_enhet", 14),
    ("lok_nr", 15),
    ("lok_navn", 16),
    ("lok_komnr", 17),
    ("lok_kom", 18),
    ("lok_plass", 19),
    ("weatherenv", 20),
    ("lok_kap", 21),
    ("lok_enhet2", 22),
    ("geowgs84_n", 23),
    ("geowgs84_e", 24),
]

org_headers = [
    ("org_nr_pers_nr", 1),
    ("name", 2),
    ("address", 3),
    ("post_code", 4),
    ("post_place", 5),
]

def create_objs(items):
    for org, data in items:
        org_data_tmp = None
        permissions = []
        for org_data, pre_data in data:
            org_data_tmp = org_data
            permissions.append(Permission.create_obj(pre_data))

        org_obj = Organization.create_obj(org_data_tmp)
        org_obj.permissions = permissions

        db_session.add(org_obj)


def csv_import(csv_path):
    with open(csv_path, "r") as fd:
        for line in fd:
            if line.startswith("#"):
                pass
            else:
                splitted = line.split(";")

                org_data = {}
                for attr, idx in org_headers:
                    org_data[attr] = splitted[idx]

                pre_data = {}
                for attr, idx in pre_headers:
                    pre_data[attr] = splitted[idx]

                # Populate table
                # If not org nr is given, use navn instead. (per nr, is sensitive information).
                if org_data["org_nr_pers_nr"] == "":
                    if items["name"].get(org_data.get("name"), None) is None:
                        items["name"][org_data.get("name")] = [(org_data, pre_data)]
                    else:
                        items["name"][org_data.get("name")].append((org_data, pre_data))
                else:
                    if items["org_nr_pers_nr"].get(org_data.get("org_nr_pers_nr"), None) is None:
                        items["org_nr_pers_nr"][org_data.get("org_nr_pers_nr")] = [(org_data, pre_data)]
                    else:
                        items["org_nr_pers_nr"][org_data.get("org_nr_pers_nr")].append((org_data, pre_data))


        # Add to db.
        create_objs(items["name"].items())
        create_objs(items["org_nr_pers_nr"].items())

        db_session.commit()


if __name__ == "__main__":
    csv_import("C:\\Users\\puse\\PycharmProjects\\nortek.no\\parsers\\data\\Akvakulturregisteret.csv")